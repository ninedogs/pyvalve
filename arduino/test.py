import serial
import struct
import time

arduino = serial.Serial("COM6", 115200)
if not arduino.isOpen():
    print("Can't open serial!")
arduino.write(struct.pack('B', 255))
time.sleep(0.5)
arduino.flush()
arduino.close()
# for i in range(255):
#     print(i)
#     arduino.write(struct.pack("B", i))
#     time.sleep(0.5)
#     arduino.flush()
#
# for i in range(255, 0, -1):
#     print(i)
#     arduino.write(struct.pack("B", i))
#     time.sleep(0.5)
#     arduino.flush()