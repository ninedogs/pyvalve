__author__ = '9dogs'

import logging
import time
import re
import struct
import serial
import threading
import queue
# from functools import wraps

DEBUG = 0

if DEBUG:
    level = logging.DEBUG
else:
    level = logging.INFO

logging.basicConfig(level=level, format='(%(asctime)s - %(name)s - %(levelname)s - %(threadName)-10s) %(message)s', )


class DeviceWhisperer(threading.Thread):
    """
    Класс для общения с девайсами
    """

    def __init__(self, com_port, name, command_queue, response_queue, loop_time=0.3):
        """
        :param com_port: экземпляр COM-порта
        :type com_port: serial.Serial
        :param name: имя треда - будет отображаться в логах
        :type name: str
        :param command_queue: очередь комманд
        :type command_queue: queue.PriorityQueue
        :param response_queue: очередь ответов на комманды
        :type response_queue: queue.Queue
        :param loop_time: минимальная задерждка между коммандами
        :type loop_time: float
        """
        super(DeviceWhisperer, self).__init__()
        self.name = name
        self.command_q = command_queue
        self.response_q = response_queue
        self.loop_time = loop_time
        self._device = com_port
        self._last_get_time = 0
        try:
            self._device.open()
        except serial.SerialException as e:
            logging.warning(e)

    def run(self):
        delay = 0.01
        while True:
            if abs(self._last_get_time - time.time()) > self.loop_time:
                # Ожидаем в очереди данные вида (priority, command, wait_response)
                _, next_command, wait_response = self.command_q.get()

                if isinstance(next_command, bytes):
                    bytes_command = next_command

                elif isinstance(next_command, str):
                    if not next_command.endswith('\n'):
                        bytes_command = (next_command + '\n').encode()
                    else:
                        bytes_command = next_command.encode()
                else:
                    # Подразумеваем int
                    bytes_command = struct.pack('B', next_command)
                try:
                    logging.debug("Sending command: {}".format(repr(next_command)))
                    self._device.write(bytes_command)
                    time.sleep(delay)
                    self._device.flush()
                    time.sleep(delay)
                    if wait_response:
                        response = self._device.readline()
                        logging.debug("Got response: {}".format(response))
                        self.response_q.put(response)
                    self._last_get_time = time.time()
                    self.command_q.task_done()
                except serial.SerialException as e:
                    logging.error(e)


class DeviceReader(threading.Thread):
    """
    Класс для общения с девайсами - только чтение из порта
    """

    def __init__(self, com_port, name, command_queue, response_queue, loop_time=0.25, delay=0.01, buf_size=11):
        """
        :param com_port: экземпляр COM-порта
        :type com_port: serial.Serial
        :param name: имя треда - будет отображаться в логах
        :type name: str
        :param command_queue: очередь комманд
        :type command_queue: queue.PriorityQueue
        :param response_queue: очередь ответов на комманды
        :type response_queue: queue.Queue

        :param loop_time: минимальная задерждка между коммандами
        :type loop_time: float
        """
        super(DeviceReader, self).__init__()
        self.name = name
        self.command_q = command_queue
        self.response_q = response_queue
        self.loop_time = loop_time
        self._device = com_port
        self._last_get_time = 0
        self.delay = delay
        self._buf_size = buf_size

        self._r = re.compile('[\d\.]+')

        try:
            self._device.open()
        except serial.SerialException as e:
            logging.warning(e)

    def run(self):
        while True:
            if not self.command_q.empty():
                if abs(self._last_get_time - time.time()) > self.loop_time:
                    _, next_command, wait_response = self.command_q.get()

                    if isinstance(next_command, bytes):
                        bytes_command = next_command

                    elif isinstance(next_command, str):
                        if not next_command.endswith('\n'):
                            bytes_command = (next_command + '\n').encode()
                        else:
                            bytes_command = next_command.encode()
                    else:
                        # Подразумеваем int
                        bytes_command = struct.pack('B', next_command)
                    try:
                        logging.debug("Sending command: {}".format(repr(next_command)))
                        self._device.write(bytes_command)
                        time.sleep(self.delay)
                        self._device.flush()
                        time.sleep(self.delay)
                        self._last_get_time = time.time()
                        self.command_q.task_done()
                    except serial.SerialException as e:
                        logging.error(e)

            elif self._device.inWaiting() == self._buf_size:
                try:
                    s = self._device.read(self._buf_size)
                    logging.debug(repr(s))
                    m = self._r.match(s[5:].decode())
                    if m:
                        sign_byte = s[4:5]
                        if sign_byte == b'\x00':
                            self.response_q.put(float(m.group()))
                        else:
                            self.response_q.put(float("-" + m.group()))

                except serial.SerialException as e:
                    logging.error(e)
            else:
                time.sleep(self.loop_time)


class AbstractBackend(object):
    """Абстрактный класс бакэнда - определяет интерфейсы"""
    def __init__(self, min_mv=0, max_mv=255):
        self.min_mv = min_mv
        self.max_mv = max_mv

    def get_pv(self):
        """
        Возвращает Process Variable от бакэнда.
        Классы-наследники должны переопределить этот метод.
        """
        raise NotImplementedError

    def set_mv(self, mv):
        """
        Задает желаемое значение контроллируемого параметра, например, положение клапана (MV - manipulated variable).
        Классы-наследники должны переопределить этот метод.
        """
        raise NotImplementedError


class LakeShoreBackend(AbstractBackend):
    """
    Пользовательский класс для работы с приборами
    """

    def __init__(self, lakeshore_port='COM7', valve_port='COM6', manometer_port='COM8', addr=12, loop_time=0.5,
                 mv_min=0, mv_max=255):
        # Настройка логов
        # self._logger = logging.getLogger(__name__)
        # self._logger.setLevel(logging.DEBUG)
        #
        # ch = logging.StreamHandler()
        # ch.setLevel(logging.DEBUG)
        # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        # ch.setFormatter(formatter)
        #
        # self._logger.addHandler(ch)

        # Минимальное время, через которое можно опрашивать LakeShore
        self.loop_time = loop_time

        # Адрес устройства
        self.addr = str(addr)

        # Команда для манометра
        self._magic_string = b'\x55\xAA\x01\x55\xAA\x01\x55\xAA\x01'

        # Типы данных, получаемые от прибора
        self._data_types = {
            # Температура. Строка вида "...+077.41K..."
            'temp': re.compile("\+([\d\.]{6})K"),
            # Мгновенная мощность нагревателя. Строка вида "0.1,99.,0.0,0,000", где последние 3 цифры - мощность
            'heater_power': re.compile("[\d\.]{3},[\d\.]{3},[\d\.]{3},\d,([\d\.]{3})")
        }
        # Опрашивать ли LakeShore для получения температуры
        self.lakeshore_poll_enabled = True

        # Ограничения для MV
        self.range = (mv_min, mv_max)

        # Мощность нагревателя
        self.heater_power = 0

        # Предыдущая температура - используется если еще не пришло время обновлять данные,
        # или по каким-то причинам не удалось получить текущее значение температуры
        # self._prev_temp = 0

        # Очередь с данными и их типами
        self.data_queue = queue.Queue()

        # Очереди для общения с устройствами
        self.lake_command_queue = queue.PriorityQueue()
        self.lake_response_queue = queue.Queue()

        self.valve_command_queue = queue.PriorityQueue()
        self.valve_response_queue = queue.Queue()

        self.manometer_command_queue = queue.PriorityQueue()
        self.manometer_response_queue = queue.Queue()

        self._valve = serial.Serial(valve_port, 115200, timeout=2, rtscts=True)
        if valve_port == lakeshore_port:
            self._lake = self._valve
        else:
            self._lake = serial.Serial(lakeshore_port, 9600, timeout=2, rtscts=True)

        self._manometer = serial.Serial(manometer_port, 9600, timeout=1)

        if not self._lake.isOpen():
            logging.critical("Can't open LakeShore serial port.")

        if not self._valve.isOpen():
            logging.critical("Can't open Valve serial port.")

        if not self._manometer.isOpen():
            logging.critical("Can't open Manometer serial port.")

        # Треды для работы с устройствами
        self.lake_control_thread = DeviceWhisperer(self._lake, "LakeThread", self.lake_command_queue,
                                                   self.lake_response_queue, loop_time=self.loop_time)
        self.lake_control_thread.setDaemon(True)
        self.lake_control_thread.start()
        self.valve_control_thread = DeviceWhisperer(self._valve, "ValveThread", self.valve_command_queue,
                                                    self.valve_response_queue, loop_time=self.loop_time)
        self.valve_control_thread.setDaemon(True)
        self.valve_control_thread.start()

        self.manometer_control_thread = DeviceReader(self._manometer, "ManThread", self.manometer_command_queue,
                                                     self.manometer_response_queue)
        self.manometer_control_thread.setDaemon(True)
        self.manometer_control_thread.start()

        # Эта штука вешает контроллер
        # self._prologix_init(addr)
        # self._check_init()
        super(LakeShoreBackend, self).__init__()

    def __enter__(self):
        return self

    def init_lake(self):
        """
        Заготовочка под отложенную инициализацию LakeShore
        """
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        При уничтожении объекта переводим LakeShore в локал, закрываем порты
        """
        self.set_lakeshore_mode(0)
        try:
            self._lake.close()
            self._valve.close()
        except Exception as e:
            logging.error(e)

    def set_valve_position(self, pos):
        """
        Устанавливает значение клапана в pos (0 - 255) - (закр. - откр.)
        :param pos: Позиция клапана (0-255)
        :type pos: int
        """
        pos = self.limit_mv(pos)
        self.valve_command_queue.put([1, struct.pack('B', pos), False])

    def _parse_response(self, response):
        """
        Определяет тип данных, исползуя self._data_types и заполняет очередь self.data_queue
        """
        try:
            response = response.decode()
        except AttributeError:
            pass

        for data_type, regex in self._data_types.items():
            match = regex.match(response)
            if match:
                logging.debug("Got type '{}' with value '{}'".format(data_type, match.group(1)))
                self.data_queue.put([data_type, match.group(1)])

    def set_lakeshore_pids(self, p, i, d, priority=50):
        """
        Устанавливает ПИДы LakeShore
        :param p: Gain (0.1 - 99) - will be interpreted as value/0.1
        :type p: float
        :param i: Reset (0.1 - 99) - (1 to 990) seconds. Set 0 to OFF.
        :type i: float
        :param d: Rate (0.1 - 99) - seconds/10. Set 0 to OFF.
        :type d: float
        """
        pid_string = "P{0:03.1f}I{1:03.1f}D{2:03.1f}\n".format(p, i, d)
        self.lake_command_queue.put([priority, pid_string, False])

    def set_lakeshore_max_power(self, max_power, priority=50):
        """
        Устанавливает максимальное значение мощности печки LakeShore.
        """
        self.lake_command_queue.put([priority, "R{}".format(max_power), False])

    def set_lakeshore_mode(self, mode, priority=50):
        """
        Устанавливает LakeShore в режим Local (0), Remote (1) или Remote with Local Lockout (2)
        :param mode: 0 - Local, 1 - Remote, 2 - Remote w/ Lock
        :type mode: int
        """
        self.lake_command_queue.put([priority, "M{}".format(int(mode)), False])

    def set_lakeshore_setpoint(self, setpoint, priority=50):
        """
        Устанавливает сетпоинт для LakeShore
        :param setpoint: setpoint [K]
        """
        self.lake_command_queue.put([priority, "S{}".format(setpoint), False])

    def set_lakeshore_heater_power(self, power):
        """
        Устанавливает вручную значение нагревателя.
        :param power: Power (0-99)
        :type power: int
        """
        # В 91C похоже не реализовано
        raise NotImplemented
        # self.safe_write"H{}\n".format(power))

    def get_lakeshore_pids(self, priority=50):
        """
        Получает от LakeShore ПИДы и возвращает (P, I, D)
        :returns: (p, i, d)
        """
        self.lake_command_queue.put([priority, 'W3', True])
        raw_string = self.lake_response_queue.get()
        logging.debug("Got raw_string {}".format(raw_string))
        try:
            p, d, i, max_power, heater = [float(x) for x in raw_string.decode().split(',')]
            return p, i, d
        except ValueError as e:
            logging.debug(e)
            return 0, 0, 0

    # def get_pv(self, priority=99):
    #     """
    #     Опрашивает LakeShore, возвращает температуру в Кельвинах
    #     Этот метод должен заботится о частоте опроса Лэйкшора
    #     :param priority: Приоритет команды
    #     :type priority: int
    #     """
    #     # Получаем температуру
    #     self.lake_command_queue.put([priority, 'WS', True])
    #     try:
    #         raw_string = self.lake_response_queue.get(False)
    #         if raw_string:
    #             self._parse_response(raw_string)
    #             self.lake_response_queue.task_done()
    #     except queue.Empty:
    #         pass

    def get_pv(self, priority=99):
        """
        Опрашивает Манометр, возвращает давление
        """
        self.lake_command_queue.put([priority, 'WS', True])
        try:
            raw_string = self.lake_response_queue.get(False)
            if raw_string:
                self._parse_response(raw_string)
                self.lake_response_queue.task_done()
        except queue.Empty:
            pass
        try:
            data = self.manometer_response_queue.get(False)
            self.data_queue.put(['pressure', data])
            self.manometer_response_queue.task_done()
        except queue.Empty:
            pass

    def get_heater_power(self, priority=100):
        """
        Получает мощность нагревателя и записывает в self.heater_power
        """
        self.lake_command_queue.put([priority, 'W3', True])
        try:
            raw_string = self.lake_response_queue.get(False)
            if raw_string:
                self._parse_response(raw_string)
                self.lake_response_queue.task_done()
        except queue.Empty:
            pass

    def set_mv(self, mv, priority=99):
        """
        Задает скважность на BeagleBoard
        Этот метод должен заботиться о частоте установки параметров
        :param mv: Manipulate Variable to set
        :type mv: int
        :param force: Force set
        :type force: bool
        """
        mv = int(self.limit_mv(mv))
        self.valve_command_queue.put([priority, struct.pack('B', mv), False])

    def limit_mv(self, mv):
        """
        Ограничивает MV в пределах range (по умолчанию от 0 до 255)
        :param mv: Varialbe to limit
        :type mv: int
        :return: Variable limited to Range (0, 255)
        """
        return max(min(mv, self.range[1]), self.range[0])

    def _prologix_init(self, addr):
        """Инициализация Prologix."""
        # TODO: разобраться, почему инициализация иногда подвисает
        logging.debug("= Begin Init =")
        logging.debug("Sending ++mode 1...")
        self._lake.write("++mode 1\n".encode())
        logging.debug("Sending ++addr {}...".format(addr))
        self._lake.write("++addr {}\n".format(addr).encode())
        logging.debug("Sending ++auto...")
        self._lake.write("++auto\n".encode())
        time.sleep(0.1)
        self._lake.flush()
        logging.debug("= End Init =")

    def _check_init(self):
        logging.debug("= Begin Check Init =")
        self._lake.write("++mode".encode())
        time.sleep(0.01)
        self._lake.flush()
        cur_mode = self._lake.readline()
        if cur_mode != "1\r\n":
            logging.warning("Mode check failed - pay attention (got {}, expect {})".format(cur_mode, "1"))

        self._lake.write("++addr".encode())
        time.sleep(0.01)
        self._lake.flush()
        cur_addr = self._lake.readline()
        if cur_addr != self.addr + "\r\n":
            logging.warning("Addr check failed - pay attention (got {}, expect {})".format(cur_addr, self.addr))


class ManometerBackend(AbstractBackend):
    def __init__(self, manometer_port='COM8', valve_port='COM6', loop_time=0.5, mv_min=0, mv_max=255):

        # Минимальное время, через которое можно опрашивать LakeShore
        self.loop_time = loop_time

        # Ограничения для MV
        self.range = (mv_min, mv_max)

        # Очередь с данными и их типами
        self.data_queue = queue.Queue()

        # Очереди для общения с устройствами
        self.manometer_command_queue = queue.PriorityQueue()
        self.manometer_response_queue = queue.Queue()

        self.valve_command_queue = queue.PriorityQueue()
        self.valve_response_queue = queue.Queue()

        self._valve = serial.Serial(valve_port, 115200, timeout=2, rtscts=True)
        if valve_port == manometer_port:
            self._manometer = self._valve
        else:
            self._manometer = serial.Serial(manometer_port, 9600, timeout=1)

        if not self._manometer.isOpen():
            logging.critical("Can't open LakeShore serial port.")

        if not self._valve.isOpen():
            logging.critical("Can't open Valve serial port.")

        # Треды для работы с устройствами
        self.manometer_control_thread = DeviceReader(self._manometer, "LakeThread", self.manometer_command_queue,
                                                     self.manometer_response_queue)
        self.manometer_control_thread.setDaemon(True)
        self.manometer_control_thread.start()
        self.valve_control_thread = DeviceWhisperer(self._valve, "ValveThread", self.valve_command_queue,
                                                    self.valve_response_queue, loop_time=self.loop_time)
        self.valve_control_thread.setDaemon(True)
        self.valve_control_thread.start()

    def set_mv(self, mv, priority=99):
        """
        Задает скважность на BeagleBoard
        Этот метод должен заботиться о частоте установки параметров
        :param mv: Manipulate Variable to set
        :type mv: int
        :param force: Force set
        :type force: bool
        """
        mv = int(self.limit_mv(mv))
        self.valve_command_queue.put([priority, struct.pack('B', mv), False])

    def limit_mv(self, mv):
        """
        Ограничивает MV в пределах range (по умолчанию от 0 до 255)
        :param mv: Varialbe to limit
        :type mv: int
        :return: Variable limited to Range (0, 255)
        """
        return max(min(mv, self.range[1]), self.range[0])

    def get_pv(self, priority=99):
        """
        Опрашивает LakeShore, возвращает температуру в Кельвинах
        Этот метод должен заботится о частоте опроса Лэйкшора
        :param priority: Приоритет команды
        :type priority: int
        """
        try:
            data = self.manometer_response_queue.get(False)
            self.data_queue.put(['pressure', data])
            self.manometer_response_queue.task_done()
        except queue.Empty:
            pass


import threading as _threading


class TestBackend(AbstractBackend):
    def __init__(self, bath=4.2, process_gain=1., dead_time=2., decay_time=5., process_period=0.05):
        self._bath = bath
        self._K = process_gain
        self._L = dead_time
        self._T = decay_time
        self._dead_periods = int(dead_time / process_period)
        self._process_period = process_period
        self._PV_list = [self._bath] * (self._dead_periods + 1)
        self.mv = 0
        self._start_process_thread()

    def get_pv(self):
        return self._PV_list[1]

    def set_mv(self, mv):
        self.mv = mv

    def __enter__(self):
        self._start_process_thread()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._stop_process = True

    def _start_process_thread(self):
        self._stop_process = False
        self._process_thread = _threading.Thread(
            target=self._run_process, name='process')
        self._process_thread.daemon = True
        self._process_thread.start()

    def _run_process(self):

        # logging.debug('\t'.join(('time', 'SP', 'PV_int', 'PV', 'dPV_bath',
        # 'dPV_drive', 'MV')))
        dt = self._process_period
        next_time = time.time() + dt
        while not self._stop_process:
            PV = self._PV_list[-1]
            dPV_bath = (self._bath - PV) / self._T * dt
            # MV = self.get_mv(_increment_i_term=True)
            MV = self.mv
            dPV_drive = self._K * MV / self._T * dt
            line = '\t'.join(str(x) for x in (time.time(), self._bath, PV, self.get_pv(),
                                              dPV_bath, dPV_drive, MV))
            # logging.debug(line + '\n')
            PV += dPV_bath + dPV_drive
            self._PV_list.pop(0)
            self._PV_list.append(PV)
            s = next_time - time.time()
            if s > 0:
                time.sleep(s)
            next_time += dt


# Тестовый запуск
if __name__ == "__main__":
    # with LakeShoreBackend() as lake:
    # for i in range(1000):
    # print(lake.get_pv())
    # time.sleep(0.01)

    # test = TestBackend()
    # test.set_mv(100)
    # for i in range(1000):
    # print(test.get_pv())
    #     time.sleep(0.1)
    # test.set_mv(0)
    # for i in range(1000):
    #     print(test.get_pv())
    #     time.sleep(0.1)

    # lake_com = serial.Serial('COM7', 9600, timeout=0.5, rtscts=True)
    # commands = queue.PriorityQueue()
    # responds = queue.Queue()
    # lake = DeviceWhisperer(lake_com, 'LakeShore', commands, responds, loop_time=0.005)
    # lake.setDaemon(True)
    # lake.start()
    # while True:
    #     commands.put((1, 'WS', True))
    #     print("Got response: {}".format(responds.get()))

    b = LakeShoreBackend('COM7', 'COM6', loop_time=0.01)
    while True:
        b.get_pv()
        try:
            print(b.data_queue.get_nowait())
        except queue.Empty:
            pass