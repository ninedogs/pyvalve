# -*- coding: utf-8 -*-
"""
Demonstrates use of PlotWidget class. This is little more than a 
GraphicsView with a PlotItem placed in its center.
"""

from pyqtgraph.Qt import QtGui, QtCore
import time
import sys
import os
import re
import pyqtgraph as pg
from numpy_buffer.ring import RingBuffer
import datetime
import logging

from backends import TestBackend, LakeShoreBackend
from pid import PIDController


class TimeAxisItem(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        # PySide's QTime() initialiser fails miserably and dismisses args/kwargs
        return [datetime.datetime.fromtimestamp(int(value)).strftime('%H:%M:%S') for value in values]


class QtGrapher(QtGui.QMainWindow):
    def __init__(self):
        super(QtGrapher, self).__init__()

        self.setpoint = 10.
        self.temp_setpoint = 8.
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.DEBUG)

        self._data_types = {
            # Температура. Строка вида "...+077.41K..."
            'temp': re.compile("\+([\d\.]{6})K"),
            # Мгновенная мощность нагревателя. Строка вида "0.1,99.,0.0,0,000", где последние 3 цифры - мощность
            'heater_power': re.compile("[\d\.]{3},[\d\.]{3},[\d\.]{3},\d,([\d\.]{3})"),
            # None - используется необработанное значение
            'pressure': None
        }

        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)

        self._logger.addHandler(ch)

        self._lakeshore_port = ''
        self._valve_port = ''
        self._manometer_port = ''

        self._backend = TestBackend()
        # self._backend = LakeShoreBackend(lakeshore_port=self._lakeshore_port, valve_port=self._valve_port)

        # Массивы данных
        self._temp_data = RingBuffer(size_max=10000)
        self._pid_data = RingBuffer(size_max=10000)
        self._heater_data = RingBuffer(size_max=10000)
        self._pressure_data = RingBuffer(size_max=10000)
        self._x_time = RingBuffer(size_max=10000)
        self._pid = PIDController(setpoint=self.setpoint)
        self._pid.reversed = True

        self.disable = False

        self._init_time = time.time()
        self._update_time = 200
        self._timer = QtCore.QTimer()
        self._timer.timeout.connect(self.update_data)
        self._init_ui()

    def _init_ui(self):
        # Параметры окна
        self.setWindowTitle('pyValve')
        self.resize(800, 600)
        central_widget = QtGui.QWidget()
        self.setCentralWidget(central_widget)

        # Главный layout - будет содержать graph_layout и sidebar_layout
        layout = QtGui.QHBoxLayout()
        central_widget.setLayout(layout)

        # Antialias для графиков
        pg.setConfigOptions(antialias=False)

        graph_v_layout = QtGui.QVBoxLayout()
        sidebar_layout = QtGui.QVBoxLayout()

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        # Группа настроек клапана
        valve_group_box = QtGui.QGroupBox("Клапан")

        # Form layout для клапана
        valve_form_layout = QtGui.QFormLayout()

        # Настройки коэфф. PID
        self._valve_p_box = QtGui.QDoubleSpinBox()
        self._valve_i_box = QtGui.QDoubleSpinBox()
        self._valve_d_box = QtGui.QDoubleSpinBox()

        for double_box in [self._valve_p_box, self._valve_i_box, self._valve_d_box]:
            double_box.setSingleStep(0.001)
            double_box.setDecimals(3)
            double_box.setRange(0., 9999.)

        # Заполняем начальные значения ПИДов клапана
        self._valve_p_box.setValue(self._pid.kp)
        self._valve_i_box.setValue(self._pid.ki)
        self._valve_d_box.setValue(self._pid.kd)

        for double_box in [self._valve_p_box, self._valve_i_box, self._valve_d_box]:
            double_box.valueChanged.connect(self._valve_pid_changed)

        valve_form_layout.addRow("P", self._valve_p_box)
        valve_form_layout.addRow("I", self._valve_i_box)
        valve_form_layout.addRow("D", self._valve_d_box)

        # Слайдер для ручного выбора позиции клапана
        self._valve_slider = QtGui.QSlider()
        self._valve_slider.setOrientation(0x1)
        self._valve_slider.setRange(0, 255)
        self._valve_slider.setSingleStep(1)

        self._valve_slider.sliderReleased.connect(self._on_valve_slider_value_changed)

        valve_form_layout.addRow(QtGui.QLabel("Ручное управление клапаном"))
        valve_form_layout.addRow(self._valve_slider)

        # Кнопки открытия/закрытия
        self._valve_close_button = QtGui.QPushButton("Close")
        self._valve_open_button = QtGui.QPushButton("Open")

        self._valve_close_button.clicked.connect(self._on_valve_close_button_clicked)
        self._valve_open_button.clicked.connect(self._on_valve_open_button_clicked)

        valve_form_layout.addRow(self._valve_close_button, self._valve_open_button)

        # Чекбокс для инвертированного вывода
        self._reversed = QtGui.QCheckBox("Инвертировать выход")
        self._reversed.setChecked(True)

        valve_form_layout.addRow(self._reversed)
        self._reversed.clicked.connect(self._on_reversed_clicked)

        # Настройки пределов работы клапана
        valve_form_layout.addRow(QtGui.QLabel("Пределы работы клапана (min, max)"))
        self._valve_min_box = QtGui.QSpinBox()
        self._valve_max_box = QtGui.QSpinBox()
        self._valve_min_box.setRange(0, 255)
        self._valve_max_box.setRange(0, 255)
        self._valve_min_box.setValue(116)
        self._valve_max_box.setValue(180)
        self._valve_min_box.valueChanged.connect(self._on_valve_range_value_changed)
        self._valve_max_box.valueChanged.connect(self._on_valve_range_value_changed)
        valve_form_layout.addRow(self._valve_min_box, self._valve_max_box)

        valve_group_box.setLayout(valve_form_layout)
        sidebar_layout.addWidget(valve_group_box)

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        # Группа настроек LakeShore
        lakeshore_group_box = QtGui.QGroupBox("LakeShore")

        # Form layout для LakeShore
        lakeshore_form_layout = QtGui.QFormLayout()

        # Настройки коэфф. PID
        self._lakeshore_p_box = QtGui.QDoubleSpinBox()
        self._lakeshore_i_box = QtGui.QDoubleSpinBox()
        self._lakeshore_d_box = QtGui.QDoubleSpinBox()
        for control in [self._lakeshore_p_box, self._lakeshore_i_box, self._lakeshore_d_box]:
            control.setRange(0, 99)
            control.setDecimals(1)
            control.setSingleStep(0.1)
            control.valueChanged.connect(self._on_lakeshore_pids_changed)

        lakeshore_form_layout.addRow("P", self._lakeshore_p_box)
        lakeshore_form_layout.addRow("I", self._lakeshore_i_box)
        lakeshore_form_layout.addRow("D", self._lakeshore_d_box)

        # Коэффициент печки (OFF, -3, -2, -1, MAX)
        self._lakeshore_maxpower_slider = QtGui.QSlider()
        self._lakeshore_maxpower_slider.setOrientation(0x1)
        self._lakeshore_maxpower_slider.setRange(1, 5)
        self._lakeshore_maxpower_slider.setSingleStep(1)
        self._lakeshore_maxpower_slider.setPageStep(1)

        self._lakeshore_maxpower_slider.sliderReleased.connect(self._on_lakeshore_maxpower_slider_value_changed)

        # вспомогательный слой
        slider_helper_layout = QtGui.QGridLayout()
        slider_helper_layout.addWidget(self._lakeshore_maxpower_slider, 0, 0, 1, 5)
        slider_helper_layout.addWidget(QtGui.QLabel("Off"), 1, 0, 1, 1)
        slider_helper_layout.addWidget(QtGui.QLabel("-3"), 1, 1, 1, 1)
        slider_helper_layout.addWidget(QtGui.QLabel("-2"), 1, 2, 1, 1)
        slider_helper_layout.addWidget(QtGui.QLabel("-1"), 1, 3, 1, 1)
        slider_helper_layout.addWidget(QtGui.QLabel("Max"), 1, 4, 1, 1)

        lakeshore_form_layout.addRow(slider_helper_layout)

        # Слайдер для ручного выбора мощности нагревателя
        self._lakeshore_heater_slider = QtGui.QSlider()
        # 0x1 - Qt::Horizontal
        self._lakeshore_heater_slider.setOrientation(0x1)
        self._lakeshore_heater_slider.setRange(0, 100)
        self._lakeshore_heater_slider.setSingleStep(1)

        lakeshore_form_layout.addRow(QtGui.QLabel("Ручное управление мощностью"))
        lakeshore_form_layout.addRow(self._lakeshore_heater_slider)

        # Кнопки Local и Remote
        self._lakeshore_remote_button = QtGui.QPushButton("Remote")
        self._lakeshore_remote_button.clicked.connect(self._on_remote_button_clicked)
        self._lakeshore_local_button = QtGui.QPushButton("Local")
        self._lakeshore_local_button.clicked.connect(self._on_local_button_clicked)

        lakeshore_form_layout.addRow(self._lakeshore_local_button, self._lakeshore_remote_button)

        lakeshore_group_box.setLayout(lakeshore_form_layout)
        sidebar_layout.addWidget(lakeshore_group_box)

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        # Настройка setpoint
        self._temp_setpoint_box = QtGui.QDoubleSpinBox()
        self._temp_setpoint_box.setRange(0, 300)
        self._temp_setpoint_box.setValue(self._pid.setpoint)
        self._temp_setpoint_box.valueChanged.connect(self._on_temp_setpoint_value_changed)

        self._pressure_setpoint_box = QtGui.QDoubleSpinBox()
        self._pressure_setpoint_box.setValue(10)
        self._pressure_setpoint_box.setRange(-300, 300)
        self._pressure_setpoint_box.valueChanged.connect(self._on_pressure_setpoint_value_changed)

        # setpoint вспомогательный слой
        setpoint_helper_layout = QtGui.QFormLayout()
        setpoint_helper_layout.addRow("Temp Setpoint", self._temp_setpoint_box)
        setpoint_helper_layout.addRow("Pressure Setpoint", self._pressure_setpoint_box)

        sidebar_layout.addLayout(setpoint_helper_layout)

        # Запись температуры
        self.log_temperature_checkbox = QtGui.QCheckBox("Записывать температуру")
        self.log_temperature_checkbox.setChecked(True)

        self.temp_log = open("temp.log", 'a')

        # Layout под кнопки запуска и остановки
        button_v_layout = QtGui.QHBoxLayout()
        # Кпонки запуска и остановки
        start_button = QtGui.QPushButton("Start")
        stop_button = QtGui.QPushButton("Stop")
        toggle_pid_button = QtGui.QPushButton("Valve PID Toggle")

        # self.d_button = QtGui.QPushButton("D")
        # button_v_layout.addWidget(self.d_button)
        # self.d_button.clicked.connect(self.dummy)

        button_v_layout.addWidget(start_button)
        button_v_layout.addWidget(stop_button)

        sidebar_layout.addLayout(button_v_layout)
        sidebar_layout.addWidget(toggle_pid_button)

        start_button.clicked.connect(self._on_start_button_clicked)
        stop_button.clicked.connect(self._on_stop_button_clicked)
        toggle_pid_button.clicked.connect(self._on_toggle_pid_button_clicked)

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        temp_plot_widget = pg.PlotWidget(name='Temperature', axisItems={'bottom': TimeAxisItem(orientation='bottom')})
        temp_plot_widget.showGrid(x=True, y=True, alpha=0.6)

        pressure_plot_widget = pg.PlotWidget(name='Pressure', axisItems={'bottom': TimeAxisItem(orientation='bottom')})
        pressure_plot_widget.showGrid(x=True, y=True, alpha=0.6)

        pid_plot_widget = pg.PlotWidget(name='PID Output', axisItems={'bottom': TimeAxisItem(orientation='bottom')})
        pid_plot_widget.showGrid(x=True, y=True, alpha=0.6)

        # Вертикальный сплитер для графиков
        graph_splitter = QtGui.QSplitter()
        graph_splitter.setOrientation(0x2)
        graph_splitter.addWidget(temp_plot_widget)
        graph_splitter.addWidget(pressure_plot_widget)
        graph_splitter.addWidget(pid_plot_widget)

        layout.addWidget(graph_splitter, 9)
        layout.addLayout(sidebar_layout, 1)

        # Кривая для температуры
        self.temp_curve = temp_plot_widget.plot()
        self.temp_curve.setPen((200, 200, 100))
        self.temp_setpoint_line = pg.InfiniteLine(angle=0, pen=(200, 200, 200))
        self.temp_setpoint_line.setValue(self.temp_setpoint)
        temp_plot_widget.addItem(self.temp_setpoint_line)

        # Кривая для давления
        pen = pg.mkPen((255, 0, 0), style=QtCore.Qt.SolidLine)
        self.pressure_curve = pressure_plot_widget.plot(pen=pen)
        self.pressure_setpoint_line = pg.InfiniteLine(angle=0, pen=(0, 200, 100))
        self.pressure_setpoint_line.setValue(self.setpoint)
        pressure_plot_widget.addItem(self.pressure_setpoint_line)

        # Кривая выхода PID-контроллера
        pen = pg.mkPen((200, 200, 100), style=QtCore.Qt.SolidLine)
        self.pid_output_curve = pid_plot_widget.plot(pen=pen)

        # Кривая мощности печки LakeShore
        pen = pg.mkPen((200, 10, 10), style=QtCore.Qt.SolidLine)
        self.lakeshore_output_curve = pid_plot_widget.plot(pen=pen)

        temp_plot_widget.setLabel('left', 'Temperature', units='K')
        temp_plot_widget.setLabel('bottom', 'Time', units='s')

        pressure_plot_widget.setLabel('left', 'Pressure', units='mbar')
        pressure_plot_widget.setLabel('bottom', 'Time', units='s')

        pid_plot_widget.setLabel('left', 'PID Output', units='Abs')
        pid_plot_widget.setLabel('bottom', 'Time', units='s')

        self._create_menu()
        self.set_lakeshore_controls(False)
        self.set_valve_controls(False)
        self.statusBar().showMessage('Ready')
        self.show()

    def _create_menu(self):
        exit_action = QtGui.QAction('&Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(self._on_exit)

        serial_ports = self._serial_ports()
        self._lakeshore_actions = []
        self._valve_actions = []
        manometer_send_string_action = QtGui.QAction("Send Remote", self)
        manometer_send_string_action.triggered.connect(self._manometer_send_string)
        self._manometer_actions = [manometer_send_string_action, ]

        for port, desc, hwid in serial_ports:
            lake_action = QtGui.QAction(port, self)
            lake_action.setCheckable(True)
            lake_action.setStatusTip('Set port to {} ({})'.format(port, desc))
            lake_action.triggered.connect(self._set_lakeshore_port)
            valve_action = QtGui.QAction(port, self)
            valve_action.setCheckable(True)
            valve_action.setStatusTip('Set port to {} ({})'.format(port, desc))
            valve_action.triggered.connect(self._set_valve_port)
            manometer_action = QtGui.QAction(port, self)
            manometer_action.setCheckable(True)
            manometer_action.setStatusTip('Set port to {} ({})'.format(port, desc))
            manometer_action.triggered.connect(self._set_manometer_port)
            self._lakeshore_actions.append(lake_action)
            self._valve_actions.append(valve_action)
            self._manometer_actions.append(manometer_action)

        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu('&File')
        lakeshore_menu = menu_bar.addMenu('LakeShore')
        valve_menu = menu_bar.addMenu('Valve')
        manometer_menu = menu_bar.addMenu('Manometer')

        file_menu.addAction(exit_action)
        if not self._lakeshore_actions or not self._valve_actions or not self._manometer_actions:
            dummy_action = QtGui.QAction("У вас нет COM-портов, поздравляю!", self)
            dummy_action.setEnabled(False)
            self._lakeshore_actions.append(dummy_action)
            self._valve_actions.append(dummy_action)
            self._manometer_actions.append(dummy_action)

        for action in self._lakeshore_actions:
            lakeshore_menu.addAction(action)

        for action in self._valve_actions:
            valve_menu.addAction(action)

        for action in self._manometer_actions:
            manometer_menu.addAction(action)

    def set_valve_controls(self, mode):
        """
        Устанавливает контролы клапаны в disabled (mode=False) или enabled (mode=True)
        """
        controls = [self._valve_slider, self._valve_close_button, self._valve_open_button]
        for control in controls:
            control.setEnabled(mode)

    def set_lakeshore_controls(self, mode):
        """
        Устанавливает контролы LakeShore в disabled (mode=False) или enabled (mode=True)
        :param mode: mode (True for Enabled, False for Disabled)
        :type mode: bool
        """
        controls = [self._lakeshore_maxpower_slider, self._lakeshore_heater_slider, self._lakeshore_remote_button,
                    self._lakeshore_local_button, self._lakeshore_p_box, self._lakeshore_i_box, self._lakeshore_d_box]
        for control in controls:
            control.setEnabled(mode)

    def _set_lakeshore_port(self):
        """
        Устанавливает COM-порт для LakeShore
        """
        self._lakeshore_port = ""
        for action in self._lakeshore_actions:
            if action.isChecked():
                self._lakeshore_port = action.text()
        self._logger.info("LakeShore port set to {}".format(self._lakeshore_port))
        self._init_devices()

    def _set_valve_port(self):
        """
        Устанавливает COM-порт для клапана
        """
        self._valve_port = ""
        for action in self._valve_actions:
            if action.isChecked():
                self._valve_port = action.text()
        self._logger.info("Valve port set to {}".format(self._valve_port))
        self._init_devices()

    def _set_manometer_port(self):
        """
        Устанавливает COM-порт для манометра
        """
        self._manometer_port = ""
        for action in self._manometer_actions:
            if action.isChecked():
                self._manometer_port = action.text()
        self._logger.info("Manometer port set to {}".format(self._manometer_port))
        self._init_devices()

    def _init_devices(self):
        """
        Инициализация бакенда, если заданы COM-порты
        """
        if self._lakeshore_port and self._valve_port and self._manometer_port:
            self._logger.debug(
                "Инициализирую LakeShore на {}, клапан на {} и манометр на {}".format(self._lakeshore_port,
                                                                                      self._valve_port,
                                                                                      self._manometer_port))
            self._backend = LakeShoreBackend(lakeshore_port=self._lakeshore_port,
                                             valve_port=self._valve_port,
                                             manometer_port=self._manometer_port,
                                             loop_time=0.03)
            # Выставляем начальные значения ПИДов
            p, i, d = self._backend.get_lakeshore_pids()
            self._lakeshore_p_box.setValue(p)
            self._lakeshore_i_box.setValue(i)
            self._lakeshore_d_box.setValue(d)
            # Выставляем setpoint
            self._on_temp_setpoint_value_changed()
            self._on_pressure_setpoint_value_changed()
            # Включаем контролы
            self.set_lakeshore_controls(True)
            self.set_valve_controls(True)
            # Включаем манометр на Remote
            self._backend.manometer_command_queue.put([1, b'\x55\xAA\x01\x55\xAA\x01\x55\xAA\x01', False])
        else:
            self._logger.debug("Инициализирую тестовый Backend {} {}".format(self._valve_port, self._lakeshore_port))
            self._backend = TestBackend()

    def _manometer_send_string(self):
        try:
            self._backend.manometer_command_queue.put([1, b'\x55\xAA\x01\x55\xAA\x01\x55\xAA\x01', False])
        except Exception as e:
            logging.error(e)

    def _on_start_button_clicked(self):
        """
        Запускает ПИД-регуляцию
        """
        self.disable = False
        # Clean queues
        with self._backend.data_queue.mutex:
            self._backend.data_queue.queue.clear()

        # Start a timer to rapidly update the plot in pw
        self._timer.start(self._update_time)

    def _on_stop_button_clicked(self):
        self.disable = True
        self._timer.stop()
        # Отключаем печку
        try:
            self._backend.set_lakeshore_max_power(1)
        except AttributeError as e:
            self._logger.error(e)

    def _on_toggle_pid_button_clicked(self):
        if self.disable:
            self.disable = False
        else:
            self.disable = True

    def update_data(self):
        """
        Обновляет данные: считает выход ПИДа, задает позицию клапана, обновляет графики
        """
        self._backend.get_pv()
        self._backend.get_heater_power()

        while not self._backend.data_queue.empty():
            local_time = time.time()
            data_type, data_value = self._backend.data_queue.get(False)
            # Добавляем x
            self._x_time.append(local_time)

            # Определяем тип данных
            if data_type not in self._data_types.keys():
                logging.error("Unexpected data type.")
                continue

            if data_type == "pressure":
                process_variable = float(data_value)
                # Смотрим, были ли пропущены какие-то значения
                skip_size = len(self._x_time) - len(self._pressure_data) - 1
                last_element = self._pressure_data[0]
                for i in range(skip_size):
                    self._pressure_data.append(last_element)
                self._pressure_data.append(process_variable)

                # error = self._pid.setpoint - process_variable
                #
                # if not self.disable:
                # pid_output = float(self._backend.limit_mv(self._pid.step_d(process_variable)))
                # else:
                # pid_output = 0

                # self._pressure_data.append({'x': local_time, 'pressure': process_variable, 'pid_y': pid_output})
                # self._pressure_data.append({'x': local_time, 'pressure': process_variable})
                # x = [item['x'] for item in self._pressure_data]
                # pressure = [float(item['pressure']) for item in self._pressure_data]
                # pid_y = [float(item['pid_y']) for item in self._pressure_data]
                # self.pid_output_curve.setData(x=x, y=pid_y)
                self.pressure_curve.setData(x=self._x_time, y=self._pressure_data)

                # self._backend.set_mv(pid_output)

                # self.statusBar().showMessage('Error: {0:06.2f}\tTemp: {1:06.2f}\tPID output: {2:06.2f}'.format(error,
                # process_variable,
                #                                                                                                pid_output))

            if data_type == "temp":
                process_variable = float(data_value)

                # Check skip data for temp array
                skip_size = len(self._x_time) - len(self._temp_data) - 1
                last_element = self._temp_data[0]
                for i in range(skip_size):
                    self._temp_data.append(last_element)
                self._temp_data.append(process_variable)

                error = self._pid.setpoint - process_variable
                if not self.disable:
                    pid_output = float(self._scale_pid_output(self._backend.limit_mv(self._pid.step_d(process_variable))))
                else:
                    pid_output = 0

                # Check skip data for pid array
                skip_size = len(self._x_time) - len(self._pid_data) - 1
                last_element = self._pid_data[0]
                for i in range(skip_size):
                    self._pid_data.append(last_element)
                self._pid_data.append(pid_output)

                self.temp_curve.setData(x=self._x_time, y=self._temp_data)
                self.pid_output_curve.setData(x=self._x_time, y=self._pid_data)
                self._backend.set_mv(pid_output)
                self.statusBar().showMessage('Error: {0:06.2f}\tTemp: {1:06.2f}\tPID output: {2:06.2f}'.format(error,
                                                                                                               process_variable,
                                                                                                               pid_output))

                if self.log_temperature_checkbox:
                    self.temp_log.write("{}\t {}".format(local_time, process_variable))
                    self.temp_log.flush()

            if data_type == "heater_power":
                heater_power = float(data_value)
                # Check skip data for heater array
                skip_size = len(self._x_time) - len(self._heater_data) - 1
                last_element = self._heater_data[0]
                for i in range(skip_size):
                    self._heater_data.append(last_element)
                self._heater_data.append(heater_power)
                self.lakeshore_output_curve.setData(x=self._x_time, y=self._heater_data)

            self._backend.data_queue.task_done()

    def _on_temp_setpoint_value_changed(self):
        """
        Обработчик нажатия кнопки сетпоинта
        """
        try:
            self._backend.set_lakeshore_setpoint(float(self._temp_setpoint_box.value()))
            self._pid.setpoint = float(self._temp_setpoint_box.value())
            self.setpoint = float(self._temp_setpoint_box.value())
            self.temp_setpoint_line.setValue(self.setpoint)
        except ValueError as e:
            logging.error(e)
        except AttributeError as e:
            self._logger.error(e)

    def _on_pressure_setpoint_value_changed(self):
        try:
            self._pid.setpoint = float(self._pressure_setpoint_box.value())
            self.setpoint = float(self._pressure_setpoint_box.value())
            self.pressure_setpoint_line.setValue(self.setpoint)
        except Exception as e:
            logging.error(e)

    def _on_valve_range_value_changed(self):
        """
        Обработчик изменения предела клапана
        """
        pass
        # try:
        #     self._backend.range = (int(self._valve_min_box.value()), int(self._valve_max_box.value()))
        # except AttributeError as e:
        #     self._logger.error(e)

    def _valve_pid_changed(self):
        """
        Обработчик изменения спинбоксов ПИДов клапана
        """
        try:
            new_Kp = float(self._valve_p_box.value())
            new_Kd = float(self._valve_d_box.value())
            new_Ki = float(self._valve_i_box.value())
        except ValueError:
            new_Kp = self._pid.kp
            new_Ki = self._pid.ki
            new_Kd = self._pid.kd
        self._pid.kp = new_Kp
        self._pid.ki = new_Ki
        self._pid.kd = new_Kd

    def _on_lakeshore_maxpower_slider_value_changed(self):
        """
        Обработчик слайдера maxpower для LakeShore
        """
        try:
            self._backend.set_lakeshore_max_power(self._lakeshore_maxpower_slider.value())
        except AttributeError as e:
            self._logger.error(e)

    def _on_valve_slider_value_changed(self):
        try:
            self.disable = True
            self._backend.set_valve_position(self._valve_slider.value())
        except AttributeError as e:
            self._logger.error(e)

    def _on_reversed_clicked(self):
        """
        Обработчик галочки "Инвертировать выход"
        """
        if self._reversed.isChecked():
            self._pid.reversed = True
        else:
            self._pid.reversed = False

    def _on_exit(self):
        self._timer.stop()
        QtGui.qApp.quit()

    def _on_local_button_clicked(self):
        try:
            self._backend.set_lakeshore_mode(0)
        except AttributeError as e:
            self._logger.error(e)

    def _on_remote_button_clicked(self):
        try:
            self._backend.set_lakeshore_mode(1)
        except AttributeError as e:
            self._logger.error(e)

    def _on_lakeshore_pids_changed(self):
        """
        Обработчик изменения спинбоксов LakeShore
        """
        try:
            self._backend.set_lakeshore_pids(self._lakeshore_p_box.value(), self._lakeshore_i_box.value(),
                self._lakeshore_d_box.value())
            self.statusBar().showMessage("New pids: {0}, {1}, {2}".format(*self._backend.get_lakeshore_pids()))
        except AttributeError as e:
            self._logger.error(e)

    def _on_valve_close_button_clicked(self):
        try:
            self.disable = True
            self._backend.set_valve_position(0)
        except AttributeError as e:
            self._logger.error(e)

    def _on_valve_open_button_clicked(self):
        try:
            self.disable = True
            self._backend.set_valve_position(255)
        except AttributeError as e:
            self._logger.error(e)

    def keyPressEvent(self, e):
        """
        Горячие клавиши
        """
        if e.key() == QtCore.Qt.Key_Escape:
            self._timer.stop()
            # self.close()
        if e.key() == QtCore.Qt.Key_1:
            self._backend.set_mv(100)
        if e.key() == QtCore.Qt.Key_2:
            self._backend.set_mv(200)
        if e.key() == QtCore.Qt.Key_3:
            self._backend.set_mv(300)

    @staticmethod
    def _serial_ports():
        """
        Lists serial ports

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of (port, desc, hwid)
        """
        ports = []
        if os.name == 'nt':  # sys.platform == 'win32':
            from serial.tools.list_ports_windows import comports
        elif os.name == 'posix':
            from serial.tools.list_ports_posix import comports
        # ~ elif os.name == 'java':
        else:
            raise ImportError("Sorry: no implementation for your platform ('%s') available" % (os.name,))
        for info in comports():
            port, desc, hwid = info
            # fixing encoding for desc
            desc = desc.encode('cp1252').decode("cp1251")
            ports.append((port, desc, hwid))
        return ports

    def dummy(self):
        self.d_button.clicked.connect(self.dummy)
        logging.error("Dummy test")

    def _scale_pid_output(self, output):
        scale_factor = abs(
            ((self._valve_min_box.value() - self._valve_max_box.value()) / self._backend.max_mv - self._backend.min_mv))
        # if self._reversed:
        #     return self._valve_max_box.value() - output * scale_factor
        # else:
        #     return self._valve_min_box.value() + output * scale_factor
        return self._valve_min_box.value() + output * scale_factor


def main():
    # level = logging.DEBUG
    level = logging.INFO
    logging.basicConfig(level=level, format='(%(asctime)s - %(name)s - %(levelname)s - %(threadName)-10s) %(message)s')
    # import sys
    app = QtGui.QApplication(sys.argv)
    grapher = QtGrapher()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
