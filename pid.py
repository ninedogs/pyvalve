import time
import logging
from collections import deque


class PIDController(object):
    """
    CO[k] = CO[k-1] + Kc([PV(k) - PV(k-1)] + 1/ti*T*e(k) + td[PV(k) - 2PV(k-1) + PV(k-2)]) (или нет)
    """

    def __init__(self, setpoint=150, Kp=110, Ki=1.6, Kd=350, min_output=0., max_output=255., min_loop_time=0.01,
                 reversed_control=False):

        # self._logger = logging.getLogger(__name__)
        # self._logger.setLevel(logging.DEBUG)
        #
        # ch = logging.StreamHandler()
        # ch.setLevel(logging.DEBUG)
        # formatter = logging.Formatter('(%(asctime)s - %(name)s - %(levelname)s) %(message)s')
        # ch.setFormatter(formatter)
        #
        # self._logger.addHandler(ch)

        # Минимальное время цикла
        self._min_loop_time = min_loop_time

        # Целевая температура
        self.setpoint = setpoint
        # Выходное значение
        self.output = 0
        # Пропорциональный коэффициент
        self.kp = Kp
        # Интегральный коэффициент
        self.ki = Ki
        # Дифференциальный коэффициент
        self.kd = Kd
        # PV[k], PV[k-1], PV[k-2]
        self._process_variable, self._process_variable_k1, self._process_variable_k2 = 0, 0, 0
        # Ошибки: e(k), e(k-1), e(k-2)
        self._error, self._error_k1, self._error_k2 = 0, 0, 0

        # Интегральный член
        self.integral_component = 0

        # Предыдущее значение выхода
        self._prev_out = 0

        # Границы выходных значений
        # self.min_out, self.max_out = min_output, max_output

        # инвертированный выход
        self.reversed = reversed_control

        # Количество точек для усреднения производной
        self.av_num = 5
        # Данные для усреднения
        self.error_data = deque(maxlen=self.av_num)

        # Границы выходных значений
        self.min_out, self.max_out = min_output, max_output

        self.range(min_output, max_output)
        self.last_update_time = time.time()

    def bound(self, output):
        """Ограничивает значение output внутри (min_out, max_out) (по умолчанию от 0 до 255)

        :param output: значение, которое нужно ограничить
        :type output: float
        :returns: ограниченное значение
        :rtype: float
        """
        return max(min(output, self.max_out), self.min_out)

    def range(self, min_output, max_output):
        """Задает границы допустимых значений для выходного значения контроллера

        :param min_output: минимальное значение
        :type min_output: float
        :param max_output: максимальное значение
        :type max_output: float
        """
        if min_output > max_output:
            (max_output, min_output) = (min_output, max_output)
        self.min_out = min_output
        self.max_out = max_output

    def step_a(self, process_input):
        """Вычисляет выходное значения PID-контроллера типа A:
        CO(k) = CO(k-1) + K_p[e(k) - e(k-1)] + K_i * T * e(k) - \frac{K_d}{T}[e(k) - 2e(k-1) + e(k-2)],
        где e(k) - SP - PV(k), - ошибка; T - период обновления цикла.

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        real_loop_time = abs(self.last_update_time - time.time())
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input

            self._error = self.setpoint - self._process_variable

            proportional_component = self.kp * (self._error - self._error_k1)
            integral_component = self.ki * real_loop_time * self._error
            differential_component = self.kd / real_loop_time * (self._error - 2 * self._error_k1 + self._error_k2)

            self.output += proportional_component + integral_component + differential_component
            self.output = self.bound(self.output)

            logging.debug(
                "T: {0:06.2f}\tP: {1:06.2f}\tI: {2:06.2f}\tD: {3:06.2f}\tO: {4:06.2f}".format(real_loop_time,
                                                                                              proportional_component,
                                                                                              integral_component,
                                                                                              differential_component,
                                                                                              self.output))

            self._process_variable_k2, self._process_variable_k1 = self._process_variable_k1, process_input

            self._error_k2, self._error_k1 = self._error_k1, self._error
            self._prev_out = self.output
            self.last_update_time = time.time()

            if self.reversed:
                return self.max_out - self.output
            else:
                return self.output

        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            if self.reversed:
                return self.max_out - self._prev_out
            else:
                return self._prev_out

    def step_b(self, process_input):
        """Вычисляет выходное значения PID-контроллера типа B:
        CO(k) = CO(k-1) + K_p[e(k) - e(k-1)] + K_i * T * e(k) - \frac{K_d}{T}[PV(k) - 2PV(k-1) + PV(k-2)],
        где e(k) - SP - PV(k), - ошибка; T - период обновления цикла.

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        real_loop_time = abs(time.time() - self.last_update_time)
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input
            self._error = self.setpoint - self._process_variable

            proportional_component = self.kp * (self._error - self._error_k1)
            integral_component = self.ki * real_loop_time * self._error

            differential_component = self.kd / real_loop_time * (self._process_variable - 2 *
                                                                 self._process_variable_k1 + self._process_variable_k2)

            self.output += proportional_component + integral_component + differential_component
            self.output = self.bound(self.output)

            logging.debug(
                "T: {0:06.2f}\tP: {1:06.2f}\tI: {2:06.2f}\tD: {3:06.2f}\tO: {4:06.2f}".format(real_loop_time,
                                                                                              proportional_component,
                                                                                              integral_component,
                                                                                              differential_component,
                                                                                              self.output))

            self._process_variable_k2, self._process_variable_k1 = self._process_variable_k1, process_input
            self._error_k2, self._error_k1 = self._error_k1, self._error
            self._prev_out = self.output
            self.last_update_time = time.time()

            if self.reversed:
                return self.max_out - self.output
            else:
                return self.output

        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            if self.reversed:
                return self.max_out - self._prev_out
            else:
                return self._prev_out

    def step_c(self, process_input):
        """Вычисляет выходное значения PID-контроллера типа C:
        CO(k) = CO(k-1) - K_p[PV(k) - PV(k-1)] + K_i * T * e(k) - \frac{K_d}{T}[PV(k) - 2PV(k-1) + PV(k-2)],
        где e(k) - SP - PV(k), - ошибка; T - период обновления цикла.

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        real_loop_time = abs(self.last_update_time - time.time())
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input
            self._error = self.setpoint - self._process_variable

            proportional_component = self.kp * (self._process_variable - self._process_variable_k1)
            integral_component = self.ki * real_loop_time * self._error
            differential_component = self.kd / real_loop_time * (self._process_variable -
                                                                 2 * self._process_variable_k1 +
                                                                 self._process_variable_k2)

            self.output += proportional_component + integral_component + differential_component
            self.output = self.bound(self.output)

            logging.info(
                "T: {0:06.2f}\tE: {1:06.2f}\tP: {2:06.2f}\tI: {3:06.2f}\tD: {4:06.2f}\tO: {5:06.2f}".format(
                    real_loop_time,
                    self._error,
                    proportional_component,
                    integral_component,
                    differential_component,
                    self.output))
            self.last_update_time = time.time()
            self._process_variable_k2, self._process_variable_k1 = self._process_variable_k1, process_input

            if self.reversed:
                return self.max_out - self.output
            else:
                return self.output

        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            if self.reversed:
                return self.max_out - self._prev_out
            else:
                return self._prev_out

    def step_d(self, process_input):
        """
        Non-velocity PID с усреднением для D

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        # TODO: сделать интегральный коэффициент через deque - integral = deque(период),
        # TODO: integral_component = sum(integral)
        if self.ki == 0:
            self.integral_component = 0
        real_loop_time = abs(time.time() - self.last_update_time)
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input
            if self.reversed:
                error = process_input - self.setpoint
            else:
                error = self.setpoint - process_input
            # error = abs(self.setpoint - process_input)
            self.error_data.append(error)

            proportional_component = self.kp * error
            self.integral_component += self.ki * real_loop_time * error

            differential_component = self._average_derivative(real_loop_time)

            # Так как в этом алгоритме интегральная компонента аддитивна, нужно ограничивать ее отдельно
            self.integral_component = self.bound(self.integral_component)

            self.output = self.bound(proportional_component + self.integral_component + differential_component)

            logging.info(
                "T: {0:06.2f}\tE: {1:06.2f}\tP: {2:06.2f}\tI: {3:06.2f}\tD: {4:06.2f}\tO: {5:06.2f}".format(
                    real_loop_time,
                    error,
                    proportional_component,
                    self.integral_component,
                    differential_component,
                    self.output))

            self._prev_out = self.output
            self.last_update_time = time.time()

            return self.output

            # if self.reversed:
            #     return self.max_out - self.output
            # else:
            #     return self.output
        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            # if self.reversed:
            #     return self.max_out - self._prev_out
            # else:
            #     return self._prev_out
            return self._prev_out

    def step_e(self, process_input):
        """
        Глупый ПИД из википедии

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        if self.ki == 0:
            self.integral_component = 0
        real_loop_time = abs(time.time() - self.last_update_time)
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input

            self._error = self.setpoint - process_input

            proportional_component = self._error
            self.integral_component += real_loop_time * self._error
            differential_component = (self._error - self._error_k1) / real_loop_time

            # Так как в этом алгоритме интегральная компонента аддитивна, нужно ограничивать ее отдельно
            # self.integral_component = self.bound(self.integral_component)

            self.output = self.bound(
                self.kp * proportional_component + self.ki * self.integral_component + self.kd * differential_component)

            logging.info(
                "T: {0:06.2f}\tE: {1:06.2f}\tP: {2:06.2f}\tI: {3:06.2f}\tD: {4:06.2f}\tO: {5:06.2f}".format(
                    real_loop_time,
                    self._error,
                    self.kp * proportional_component,
                    self.ki * self.integral_component,
                    self.kd * differential_component,
                    self.output))

            self._error_k1 = self._error
            self._prev_out = self.output
            self.last_update_time = time.time()

            if self.reversed:
                return self.max_out - self.output
            else:
                return self.output

        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            if self.reversed:
                return self.max_out - self._prev_out
            else:
                return self._prev_out

    def step_f(self, process_input):
        """
        Глупый ПИД из википедии + усреднение для D

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        if self.ki == 0:
            self.integral_component = 0
        real_loop_time = abs(time.time() - self.last_update_time)
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input
            self.error_data.append(process_input)

            self._error = self.setpoint - process_input

            proportional_component = self._error
            self.integral_component += real_loop_time * self._error
            if len(self.error_data) == self.av_num:
                differential_component = self.kp * self.kd / real_loop_time / self.av_num * (
                    self.error_data[self.av_num - 1] - self.error_data[0])
            else:
                differential_component = 0

            # Так как в этом алгоритме интегральная компонента аддитивна, нужно ограничивать ее отдельно
            # self.integral_component = self.bound(self.integral_component)

            self.output = self.bound(
                self.kp * proportional_component + self.ki * self.integral_component + self.kd * differential_component)

            logging.info(
                "T: {0:06.2f}\tE: {1:06.2f}\tP: {2:06.2f}\tI: {3:06.2f}\tD: {4:06.2f}\tO: {5:06.2f}".format(
                    real_loop_time,
                    self._error,
                    self.kp * proportional_component,
                    self.ki * self.integral_component,
                    self.kd * differential_component,
                    self.output))

            self._error_k1 = self._error
            self._prev_out = self.output
            self.last_update_time = time.time()

            if self.reversed:
                return self.max_out - self.output
            else:
                return self.output

        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            if self.reversed:
                return self.max_out - self._prev_out
            else:
                return self._prev_out

    def step_g(self, process_input):
        """Вычисляет выходное значения PID-контроллера типа C:
        CO(k) = CO(k-1) - K_p[PV(k) - PV(k-1)] + K_i * T * e(k) - \frac{K_d}{T}[PV(k) - 2PV(k-1) + PV(k-2)],
        где e(k) - SP - PV(k), - ошибка; T - период обновления цикла. + усреднение D

        :param process_input: значение контроллируемой величины
        :type process_input: float
        :returns: выходное значение
        :rtype: float
        """
        real_loop_time = abs(self.last_update_time - time.time())
        if real_loop_time > self._min_loop_time:
            self._process_variable = process_input
            self._error = self.setpoint - self._process_variable
            self.error_data.append(process_input)

            proportional_component = self.kp * (self._process_variable - self._process_variable_k1)
            integral_component = self.ki * real_loop_time * self._error
            if len(self.error_data) == self.av_num:
                differential_component = self.kp * self.kd / real_loop_time / self.av_num * (
                    self.error_data[self.av_num - 1] - self.error_data[0])
            else:
                differential_component = 0
            # differential_component = self.kd / real_loop_time * (self._process_variable -
            #                                                      2 * self._process_variable_k1 +
            #                                                      self._process_variable_k2)

            self.output += proportional_component + integral_component - differential_component
            self.output = self.bound(self.output)

            logging.info(
                "T: {0:06.2f}\tE: {1:06.2f}\tP: {2:06.2f}\tI: {3:06.2f}\tD: {4:06.2f}\tO: {5:06.2f}".format(
                    real_loop_time,
                    self._error,
                    proportional_component,
                    integral_component,
                    -differential_component,
                    self.output))
            self.last_update_time = time.time()
            self._process_variable_k2, self._process_variable_k1 = self._process_variable_k1, process_input

            if self.reversed:
                return self.max_out - self.output
            else:
                return self.output

        else:
            logging.debug("Idle run, returning prev_out {}".format(self._prev_out))
            if self.reversed:
                return self.max_out - self._prev_out
            else:
                return self._prev_out

    def _average_derivative(self, real_loop_time):
        if len(self.error_data) == self.av_num:
            # mean_index = int(len(self.error_data)/2)
            # first_part = self.error_data[:mean_index]
            # last_part = self.error_data[mean_index:]
            # x1 = sum(first_part)/mean_index
            # x2 = sum(last_part)/mean_index
            # d = self.kd / real_loop_time * (x2 - x1)
            d = self.kd / real_loop_time / self.av_num * (
                self.error_data[self.av_num - 1] - self.error_data[0])
        else:
            d = 0
        return d

    def measure(self):
        pass


if __name__ == "__main__":
    pid = PIDController()
    time.sleep(1)
    pid.step_c(100)